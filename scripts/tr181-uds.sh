#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}

case $1 in
    start|boot)
        source /etc/environment
        tr181-uds -D
        ;;
    stop)
        if [ -f /var/run/tr181-uds.pid ]; then
            kill `cat /var/run/tr181-uds.pid`
        fi
        ;;
    debuginfo)
        ubus-cli "UnixDomainSockets.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
        echo "TODO log tr181 uds client"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
