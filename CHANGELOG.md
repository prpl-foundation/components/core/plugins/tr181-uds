# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.1 - 2024-04-10(07:12:08 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.2.0 - 2024-03-25(07:35:56 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.1.2 - 2023-09-28(15:17:04 +0000)

### Other

- Opensource component

## Release v0.1.1 - 2023-09-07(12:58:02 +0000)

### Other

- Make components available on gitlab.softathome.com

## Release v0.1.0 - 2023-07-03(15:33:47 +0000)

### New

- [IMTP] Implement IMTP communication as specified in TR-369

