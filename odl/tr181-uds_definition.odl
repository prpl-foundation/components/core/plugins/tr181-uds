%define {
    /**
     * This object contains information related to the Unix Domain Sockets used by USP Agent UDS MTP.
     * @version 2.16.0
     */
    %persistent object UnixDomainSockets {
        /**
         * This object contains parameters relating to a UnixDomainSocket configuration.
         *
         * At most one entry in this table can exist with a given value for Alias, or with a given
         * value for Path.
         * @version 2.16.0
         */
        %persistent %read-only object UnixDomainSocket[] {
            counted with UnixDomainSocketNumberOfEntries;

            /**
             * A non-volatile unique key used to reference this instance. Alias provides a mechanism
             * for a Controller to label this instance for future reference.
             *
             * The following mandatory constraints MUST be enforced:
             *
             * The value MUST NOT be empty.
             * The value MUST start with a letter.
             * If the value is not assigned by the Controller at creation time, the Agent MUST 
             * assign a value with an "cpe-" prefix. 
             *
             * This is a non-functional key and its value MUST NOT change once it's been assigned by
             * the Controller or set internally by the Agent.
             * @version 2.16.0
             */
            %persistent %unique %key string Alias;

            /**
             * Describes how the Unix Domain Socket must be used. Enumeration of:
             *
             * - Listen (UNIX Domain Socket Server)
             * - Connect (UNIX Domain Socket Client)
             * @version 2.16.0
             */
            %persistent %read-only string Mode;

            /**
             * File path of the Unix Domain Socket.
             *
             * Example: /var/run/usp/broker-controller
             * @version 2.16.0
             */
            %persistent %read-only string Path;
        }
    }
}